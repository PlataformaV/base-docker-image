# PV Base Docker Images

## PHP Apache Image

*Não possui supervisor, mas pode ser instalado manualmente e iniciado no ENTRYPOINT

```Text
registry.gitlab.com/plataformav/base-docker-image:php-7.0-apache
registry.gitlab.com/plataformav/base-docker-image:php-7.1-apache
registry.gitlab.com/plataformav/base-docker-image:php-7.2-apache
registry.gitlab.com/plataformav/base-docker-image:php-7.3-apache
registry.gitlab.com/plataformav/base-docker-image:php-7.4-apache
registry.gitlab.com/plataformav/base-docker-image:php-8.0-apache
registry.gitlab.com/plataformav/base-docker-image:php-8.1-apache
```

Exemplo Dockerfile para o projeto

```Dockerfile
FROM registry.gitlab.com/plataformav/base-docker-image:php-8.1-apache

# Configurações adicionais aqui
# Instalação de dependencias do PHP aqui
# RUN set -ex; \
#     apt install xny \
#     && install-php-extensions pdo_mysql

COPY . ./

RUN composer install -o -vvv --prefer-dist --no-interaction --no-progress --no-scripts --no-suggest --optimize-autoloader -a
```

## PHP FPM/Nginx Images

```Text
registry.gitlab.com/plataformav/base-docker-image:php-7.0-fpm
registry.gitlab.com/plataformav/base-docker-image:php-7.1-fpm
registry.gitlab.com/plataformav/base-docker-image:php-7.2-fpm
registry.gitlab.com/plataformav/base-docker-image:php-7.3-fpm
registry.gitlab.com/plataformav/base-docker-image:php-7.4-fpm
registry.gitlab.com/plataformav/base-docker-image:php-8.0-fpm
registry.gitlab.com/plataformav/base-docker-image:php-8.1-fpm
```

```Dockerfile
FROM registry.gitlab.com/plataformav/base-docker-image:php-8.1-fpm

# Configurações adicionais aqui
# Instalação de dependencias do PHP aqui
# RUN set -ex; \
#     apt install xny \
#     && install-php-extensions pdo_mysql

COPY . ./

# Para configuração adicional do supervisor
COPY supervisor.conf /etc/supervisor/conf.d/

RUN composer install -o -vvv --prefer-dist --no-interaction --no-progress --no-scripts --no-suggest --optimize-autoloader -a
```
